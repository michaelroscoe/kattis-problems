/**
 * This program solves the Zoo problem on Kattis
*/
import java.util.ArrayList;
import java.util.Collections;
import java.util.Hashtable;
import java.util.Scanner;
public class Zoo {

    public static void main (String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = Integer.parseInt(sc.nextLine());
        int caseCounter = 0;
        while (n != 0 && caseCounter < 5) {
            caseCounter++;
            Hashtable<String, Animal> animals = new Hashtable<>();
            ArrayList<String> names = new ArrayList<>();
            for (int i = 0; i < n; i++) {
                String line = sc.nextLine();
                String[] words = line.split(" ");
                String animal = words[words.length-1].toLowerCase();
                Animal x = new Animal(animal);
                if(animals.containsKey(animal)) {
                    animals.get(animal).count++;
                } else {
                    animals.put(animal, x);
                }
                if (!names.contains(animal)) {
                    names.add(animal);
                }

            }
            System.out.println("List " + caseCounter + ":");
            Collections.sort(names);
            for (String s: names) {
                System.out.println(s + " | " + animals.get(s).count);
            }
            n = Integer.parseInt(sc.nextLine());
        }
    }

}
class Animal {
    public int count;
    public String name;

    public Animal (String n) {
        name = n;
        count = 1;
    }
}