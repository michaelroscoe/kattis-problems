import java.util.Scanner;

/**
 * Solution for the problem 'A new alphabet' on the Kattis
 * Judge platform
 */
public class Alphabet {

    public static void main (String[] args) {

        Scanner sc = new Scanner(System.in);

        String input = sc.nextLine();
        input = input.toLowerCase();

        String output = "";

        for (int i = 0; i < input.length(); i++) {
            char ch = input.charAt(i);
            switch (ch) {
                case 'a':
                    output += "@";
                    break;
                case 'b':
                    output += "8";
                    break;
                case 'c':
                    output += "(";
                    break;
                case 'd':
                    output += "|)";
                    break;
                case 'e':
                    output += "3";
                    break;
                case 'f':
                    output += "#";
                    break;
                case 'g':
                    output += "6";
                    break;
                case 'h':
                    output += "[-]";
                    break;
                case 'i':
                    output += "|";
                    break;
                case 'j':
                    output += "_|";
                    break;
                case 'k':
                    output += "|<";
                    break;
                case 'l':
                    output += "1";
                    break;
                case 'm':
                    output += "[]" + (char)92 + "/[]";
                    break;
                case 'n':
                    output += "[]" + (char)92 + "[]";
                    break;
                case 'o':
                    output += "0";
                    break;
                case 'p':
                    output += "|D";
                    break;
                case 'q':
                    output += "(,)";
                    break;
                case 'r':
                    output += "|Z";
                    break;
                case 's':
                    output += "$";
                    break;
                case 't':
                    output += "']['";
                    break;
                case 'u':
                    output += "|_|";
                    break;
                case 'v':
                    output += (char) 92 + "/";
                    break;
                case 'w':
                    output += (char) 92 + "/" + (char) 92 + "/";
                    break;
                case 'x':
                    output += "}{";
                    break;
                case 'y':
                    output += "`/";
                    break;
                case 'z':
                    output += "2";
                    break;
                default:
                    output += ch;
            }
        }

        System.out.print(output);

    }
}
